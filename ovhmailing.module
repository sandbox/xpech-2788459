<?php
/**
	This file is part of ovhmailinglist drupal(c) module.

	ovhmailinglist is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ovhmailinglist is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <http://www.gnu.org/licenses/>

	Original author : Xavier Péchoultres <x.pechoultres@expert-solutions.fr>

*/
require(dirname(__FILE__).'/ovhapi.php');

/**
 * Implements hook_help().
 *
 * Displays help and module information.
 *
 * @param path 
 *	Which path of the site we're using to display help
 * @param arg 
 *	Array that holds the current path as returned from arg() function
 */
function ovhmailing_help($path, $arg) {
	switch ($path) {
		case "admin/help#ovhmailing":
		return '';
		break;
	}
}

/**
 * Implements hook_block_info().
 */
function ovhmailing_block_info() {
	$blocks['ovhmailing'] = array(
		// The name that will appear in the block list.
		'info' => t('Mailinglists'),
		// Default setting.
		'cache' => DRUPAL_CACHE_PER_ROLE,
	);
	return $blocks;
}
/**
 * Implements hook_permission().
 */
function ovhmailing_permission() {
	return array(
		'manage ovh mailing lists' => array(
			'title' => t('Access to the OVH Mailing lists module'),
			)
		);
}

/**
 * Implements hook_menu().
 */
function ovhmailing_menu() {
	$items = array();

	$items['ovhmailing/lists'] = array(
		'title' => 'Mailing lists',
		'page callback' => 'mailinglists',
		'access arguments' => array('manage ovh mailing lists'),
		'type' => MENU_CALLBACK,
	);
/*	$items['ovhmailing/list/%list-name'] = array(
		'title' => 'Mailing list',
		'page callback' => 'mailinglist',
		'access arguments' => array('access content'),
		'type' => MENU_CALLBACK,
	);*/

	$items['admin/config/content/ovhmailing'] = array(
		'title' => 'OVH Mailing list',
		'description' => 'Configuration for OVH Mailing list module',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('ovhmailing_form'),
		'access arguments' => array('access administration pages'),
		'type' => MENU_NORMAL_ITEM,
	);

	return $items;
}


function mailinglists($list=null) {
	
	$ovh = new OvhApi();
	$html='';
	$vars = array();
	$pending_delete = array();
	$html = '';
	if ($list) {
		if (array_key_exists('new_suscriber_email',$_POST)) {
			$html = 'add email ...';
			$r = $ovh->mailinglistAddSubscribers($list,$_POST["new_suscriber_email"]); 
			$html .= print_r($r,true);
		}
		if (array_key_exists('remove_suscriber',$_POST) && array_key_exists('suscribers',$_POST)) {
			$html = 'remove email ...';
			foreach($_POST['suscribers'] as $email)
			{
				$pending_delete[] = $email;
				$r = $ovh->mailinglistDeleteSubscribers($list,$email); 
			}
		}

		$t = $ovh->mailinglistsubscribers($list); 
		$html .= '<form method="POST"><label id="new_suscriber_email">'. t('Add new suscriber :'). '</label>
			<input type="text" id="new_suscriber_email" name="new_suscriber_email"/>
			<input type="submit" name="new_suscriber" value="'. t('Add') .'"/>';
			
		$rows = array();
		if ($t) {
			sort($t);
			foreach($t as $l)
			{
				$rows[] = array('<input type="checkbox" name="suscribers[]" value="'. $l .'"></td>',
							$l,
							(in_array($l,$pending_delete) ? t('deleting ...') : ''));
			}
		}
		$html .= theme('table', array(/* 'header' => $header, */
					'rows' => $rows ));
		
		$html .= '<input type="submit" name="remove_suscriber" value="'. t('Remove selected') .'"/>';
		$html .= '</form>';
		return $html;
		
	} else {
		$base ='/ovhmailing/lists/'; // drupal_get_path("module",'ovhmailing');
		
		$vars['mailinglists'] = array(
			'#type' => 'table',
			'title' => 'Mailing lists'
		);
		$t = $ovh->mailinglists(); 
		$rows = array();
		if ($t) {
			foreach($t as $l)
			{
				$rows[] = array($l,'<a href="'. $base .  $l.'">'.t('edit').'</a>');
			}
		}
		$vars['mailinglists']['rows'] = $rows;
		$html .=  theme('table', array(/* 'header' => $header, */
					'rows' => $rows ));
	}
	return $html;
}

/**
 * Page callback: Current posts settings
 *
 * @see current_posts_menu()
 */
function ovhmailing_form($form, &$form_state) {
	$form['ovhmailing_info'] = array(
		'#type' => 'item',
		'#description' => t('Thanks for providing both your school and your country.'),
	);
	$form['ovhmailing_domain'] = array(
		'#type' => 'textfield',
		'#title' => t('Domain'),
		'#default_value' => variable_get('ovhmailing_domain', 'your.domain'),
		'#size' => 40,
		'#maxlength' => 40,
		'#description' => t('Your mailing list domain (mydomain.com). '),
		'#required' => TRUE,
	);
	$form['ovhmailing_app_key'] = array(
		'#type' => 'textfield',
		'#title' => t('Application key'),
		'#default_value' => variable_get('ovhmailing_app_key', 'your_app_key'),
		'#size' => 40,
		'#maxlength' => 40,
		'#description' => t('Get it at ') . '<a href="https://eu.api.ovh.com/createApp/" target="_blank">OVH API</a>',
		'#required' => TRUE,
	);

	$form['ovhmailing_app_secret'] = array(
		'#type' => 'textfield',
		'#title' => t('Secret key'),
		'#default_value' => variable_get('ovhmailing_app_secret', 'your_app_secret'),
		'#size' => 40,
		'#maxlength' => 40,
		'#description' => t('Provided by OVH on app creation.'),
		'#required' => TRUE,
	);

	$form['ovhmailing_consumer_key'] = array(
		'#type' => 'textfield',
		'#title' => t('Consumer key'),
		'#default_value' => variable_get('ovhmailing_consumer_key', 'your_consumer_key'),
		'#size' => 40,
		'#maxlength' => 40,
		'#description' => t('Your consumer key'),
		'#required' => FALSE,
	);

	$form['ovhmailing_get_key'] = array(
		'#type' => 'checkbox',
		'#title' => t('New customer key'),
		'#default_value' => false,
		'#description' => t('Check to request a customer key'),
		'#required' => FALSE,
	);

	$form['ovhmailing_url'] = array(
		'#type' => 'textfield',
		'#title' => t('API URL'),
		'#default_value' => variable_get('ovhmailing_url', 'https://api.ovh.com/1.0'),
		'#size' => 30,
		'#maxlength' => 30,
		'#description' => t('API URL'),
		'#required' => TRUE,
	);

	return system_settings_form($form);
}


/**
 * Implements validation from the Form API.
 * 
 * @param $form
 *	A structured array containing the elements and properties of the form.
 * @param $form_state
 *	An array that stores information about the form's current state 
 *	during processing.
 */
function ovhmailing_form_validate($form, &$form_state){
	if ($form_state['values']['ovhmailing_get_key'])
	{
		$ovh = new OvhApi();
		$cred = $ovh->requestCredentials();
		
		if (isset($ovh->consumer_key)) {
			$form_state['values']['ovhmailing_consumer_key'] = $ovh->consumer_key;
		}
		if (isset($ovh->validationUrl)) {
			form_set_error('ovhmailing_get_key', t('You need to validate your request whith this link : <a href="!link" target="_blank">here</a>.',
						array('!link' => $ovh->validationUrl)));
		}
	}
}