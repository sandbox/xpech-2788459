<?php
/**
	This file is part of ovhmailinglist drupal(c) module.

	ovhmailinglist is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ovhmailinglist is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Foobar.  If not, see <http://www.gnu.org/licenses/>

	Original author : Xavier Péchoultres <x.pechoultres@expert-solutions.fr>

	Use some code of https://github.com/ovh/php-ovh
*/


class OvhApi {
	var $url;
	var $timedelta = null;
	var $application_key = null;
	var $consumer_key = null;
	var $application_secret = null;
	var $domain;
	
	function __construct() {
		$this->url = variable_get('ovhmailing_url',null);
		$this->application_key = variable_get('ovhmailing_app_key',null);
		$this->application_secret = variable_get('ovhmailing_app_secret',null);
		$this->consumer_key = variable_get('ovhmailing_consumer_key',null);
		$this->domain = variable_get('ovhmailing_domain',null);
	}
	
	public function rawCall($method,$action,$data=null,$is_authenticated = true)
	{
		$url = $this->url . $action;
		$opt = array();
		$method = strtoupper($method);
		$opt['method'] = $method;
		
		$body  ='';
		if (is_array($data)) {
			
			if ($opt['method'] == 'GET')
			{
				$opt['data'] = drupal_http_build_query($data);
				
			} else 
			{
				$body = json_encode($data);
				$body = str_replace('\\/','/',$body);
				$opt['data'] = $body;
			}
			
		}
		
		$headers = array();
		$headers['Content-Type'] = 'application/json; charset=utf-8';
		if ($this->application_key)
			$headers['X-Ovh-Application'] = $this->application_key;

		if ($is_authenticated) {
			$delta =  $this->timeDelta();
			$now = time() + $delta;
			$headers['X-Ovh-Delta'] = $delta;
			$headers['X-Ovh-Timestamp'] = $now;
			if (isset($this->consumer_key)) {
				$toSign = $this->application_secret . '+' . $this->consumer_key . '+' . $method . '+' . $url . '+' . $body . '+' . $now;
				$headers['X-Ovh-Signature3']  = $toSign;
				$headers['X-Ovh-Consumer']  = $this->consumer_key;
				$headers['X-Ovh-Signature'] = '$1$' . sha1($toSign);
				}
		}
		$opt['headers'] = $headers;
		
		$res = drupal_http_request($url,$opt);
		return $res;
	}
	/**
	* Wrap call to Ovh APIs for DELETE requests
	*
	* @param string $path    path ask inside api
	* @param array  $content content to send inside body of request
	*
	* @return array
	* @throws \GuzzleHttp\Exception\ClientException if http request is an error
	*/
	public function delete($path, $content = null)
	{
		return $this->decodeResponse(
			$this->rawCall("DELETE", $path, $content));
	}
		
	public function mailinglists()
	{
		if ($this->domain)
		{	$action =  sprintf('/email/domain/%s/mailingList',$this->domain);
			$req =  $this->rawCall('GET',$action);
			if (isset($req->data)) return $this->decodeResponse($req);
		}
		return null;
	}
	
	public function mailinglist($list)
	{
		$action =  sprintf('/email/domain/%s/mailingList/%s',$this->domain,$list);
		$req =  $this->rawCall('GET',$action);
		if (isset($req->data)) return $this->decodeResponse($req);
		return null;
	}
	
	public function mailinglistsubscribers($list)
	{
		$action =  sprintf('/email/domain/%s/mailingList/%s/subscriber',$this->domain,$list);
		$req =  $this->rawCall('GET',$action);
		if (isset($req->data)) return $this->decodeResponse($req);
		return null;
	}
	public function mailinglistAddSubscribers($list,$email)
	{
		$action =  sprintf('/email/domain/%s/mailingList/%s/subscriber',$this->domain,$list);
		$req =  $this->rawCall('POST',$action,array('email' => $email));
		if (isset($req->data)) return $this->decodeResponse($req);
		return null;
	}
	public function mailinglistDeleteSubscribers($list,$email)
	{
		$action =  sprintf('/email/domain/%s/mailingList/%s/subscriber/%s',$this->domain,$list,$email);
		$req =  $this->rawCall('DELETE',$action);
		if (isset($req->data)) return $this->decodeResponse($req);
		return null;
	}
	

	
	public function timeDelta()
	{
		if (!isset($this->timedelta )) {
			$response = $this->rawCall('GET','/auth/time',null,false);
			$serverTimestamp  = (int)(string)$response->data;
			$this->timedelta = $serverTimestamp - (int)time();
		}
		return $this->timedelta;
	}
	
	private function decodeResponse($response)
	{
//		printf('<pre>response : %s</pre>',print_r($response,true));
		return json_decode($response->data, true);
	}
	
	public function requestCredentials($accessRules = null,$redirection = null) {
		$datas = array();
		if ($accessRules  == null) {
			$accessRules = 
				array( array('method' => 'DELETE','path' => '/*'),
						array('method' => 'GET','path' => '/*'),
						array('method' => 'POST','path' => '/*'),
						array('method' => 'PUT','path' => '/*')
				);
		}
		$datas['accessRules'] = $accessRules;
		if ($redirection) $datas['redirection'] = $redirection;
		
		$response = $this->decodeResponse(
				$this->rawCall(
					'POST',
					'/auth/credential',
					$datas,
					false
				)
			);
		if (array_key_exists('validationUrl',$response)) {
			$this->validationUrl = $response['validationUrl'];
		}
		if (array_key_exists('consumerKey',$response)) {
			$this->consumer_key = $response['consumerKey'];
		}
		return $response;
	}
}